# i3pystatus needs to be run in a pipx managed virtual environment
# install pipx:
#    sudo apt install pipx

# install i3pystatus using pipx:
#    pipx install git+https://github.com/enkore/i3pystatus.git@current

# install i3pystatus dependencies:
#    cat ~/.dot-files/requires/i3pystatus.txt | sed -e 's/#.*//' | xargs pipx inject i3pystatus

from i3pystatus import Status

status = Status()

status.register(
    "clock",
    format="%H:%M:%S %m-%d-%Y",
)

status.register(
    "temp",
    format="CPU: {Package_id_0}°C",
    lm_sensors_enabled=True,
    dynamic_color=True,
    interval=1,
)

status.register(
    "gpu_temp",
    format="GPU: {temp}°C",
    interval=1,
)

# requires libpulseaudio
status.register(
    "pulseaudio",
    format="♪{volume}",
)

status.register(
    "network",
    #  Attempt to detect the active interface
    detect_active=True,
    #  If the interface is in unknown state, display it as if it were up
    unknown_up=True,
    #  Change to next interface if current one is down
    next_if_down=True,
    #  interval in seconds between module updates
    interval=1,
)

status.run()
