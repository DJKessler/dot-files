#!/bin/sh

session="sys-mon"

# set up tmux
tmux start-server

# create a new tmux session
tmux new-session -d -s $session -n $session

# create horizontal split
tmux split-window -h

# start htop
tmux send-keys "htop" C-m

# start journalctl on pane 1
tmux select-pane -t 1
tmux send-keys "journalctl --follow" C-m

# follow i3log on pane 3
tmux split-window -v
tmux send-keys "tail --follow ~/.i3log" C-m
