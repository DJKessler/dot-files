if [ -f "$HOME/.dot-files/rsync.excludes" ]; then
  alias rsync="rsync --exclude-from=$HOME/.dot-files/rsync.excludes"
fi
