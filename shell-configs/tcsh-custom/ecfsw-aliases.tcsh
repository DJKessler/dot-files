################################################################################
#
# Aliases for Europa Flight Software Development
#
################################################################################

setenv ALIAS_LOC "/proj/fswcore/env/ghscripts"

set PROJ_DEV="/proj/fswcore/fsw/dev/${USER}"
setenv PROJ_DEV ${PROJ_DEV}

set GDBHISTFILE="${PROJ_DEV}/.gdb-history"
setenv GDBHISTFILE ${GDBHISTFILE}

set GCC_COLORS='error=01;41:warning=01;93:note=01;36:caret=01;32:locus=01:quote=01'
setenv GCC_COLORS ${GCC_COLORS}

alias indir 'setenv _sd_ `pwd` \\
  && echo "Entering \!:1"      \\
  && cd \!:1                   \\
  && \!:2*                     \\
; echo "Returning to ${_sd_}"  \\
; cd ${_sd_}'

alias set-sandbox 'setenv _sd_ `pwd` \\
  && cd \!:1                         \\
  && set SANDBOX=`pwd`               \\
  && setenv SANDBOX ${SANDBOX}       \\
; cd ${_sd_}'

alias sdd2pdf 'indir ${SANDBOX}/\!:1/doc \\
  sed "/<script/,/<[/]script>/d"  \\
    \!:1.html > \!:1_nojava.html; \\
    /usr/bin/wkhtmltopdf --orientation Landscape --page-height 350 --page-width 500 \\
    \!:1_nojava.html              \\
    \!:1_sdd.pdf;                 \\
    rm \!:1_nojava.html'

if ( ! $?SANDBOX ) then
    set-sandbox "${PROJ_DEV}/sandbox-default/src"
endif

alias ghupdate 'setenv _sd_ `pwd` \\
  && cd ${SANDBOX} \\
  && (ghguard) \\
  && (git pull >& /dev/null || (${ALIAS_LOC}/git-pull-warning ; git pull ; true)) \\
  && git submodule foreach --quiet --recursive "git pull &> /dev/null || (${ALIAS_LOC}/git-pull-warning && git pull ; true)" \\
  && echo "GHUPDATE - updates complete." \\
; cd ${_sd_}'

alias ecfsw-mod-ut-build 'indir ${SANDBOX} ./zbit_mk/scripts/build_ut.sh \!:1'

alias ecfsw-mod-ut-run 'setenv _sd_ `pwd` \\
  && echo "Entering ${SANDBOX}"           \\
  && cd ${SANDBOX}                        \\
  && ./zbit_mk/scripts/build_ut.sh \!:1   \\
  && make -C \!:1 run_ut_linux            \\
; echo "Returning to ${_sd_}"             \\
; cd ${_sd_}'

alias ecfsw-mod-ut-gdb 'gdb --tui --quiet -cd=${SANDBOX}/\!:1/test/ut   \\
    ${SANDBOX}/../obj/linux-ut-x86-debug-gnu-4.4.0/\!:1/test/ut/test_ut \\
    -tty=\!:2                                                           \\
    -ex "break uth_incr_test_error_count"'

alias ecfsw-mod-ut-cov 'setenv _sd_ `pwd` \\
  && echo "Entering ${SANDBOX}"           \\
  && cd ${SANDBOX}                        \\
  && ./zbit_mk/scripts/build_ut.sh \!:1   \\
  && make -C \!:1 run_ut_linux            \\
  && make -C \!:1 cov_app                 \\
; echo "Returning to ${_sd_}"             \\
; cd ${_sd_}'

alias ecfsw-clean 'make -C ${SANDBOX}/build_eurc clean_all'

alias ecfsw-build 'make -C ${SANDBOX}/build_eurc'

alias ecfsw-build-wsts 'make -C ${SANDBOX}/build_eurc wsts'

alias ecfsw-clean-build 'make -C ${SANDBOX}/build_eurc clean_all && make -C ${SANDBOX}/build_eurc'

alias ecfsw-mod-merge 'setenv _sd_ `pwd`                         \\
  && echo "Entering ${SANDBOX}/\!:1"                             \\
  && cd ${SANDBOX}/\!:1                                          \\
  && (ghguard)                                                   \\
  && git checkout master                                         \\
  && git pull                                                    \\
  && git merge \!:2 --no-ff -m "Merging \!:2 branch with master" \\
; echo "Returning to ${_sd_}"                                    \\
; cd ${_sd_}'

alias ecfsw-break-build 'setenv _sd_ `pwd`                       \\
  && echo "Entering ${SANDBOX}"                                  \\
  && cd ${SANDBOX}                                               \\
  && ghupdate                                                    \\
  && make -C ${SANDBOX}/build_eurc clean_all                     \\
  && make -C ${SANDBOX}/build_eurc                               \\
  && setenv PATH ${SANDBOX}/dpm_svc/test/ut/support:$PATH        \\
  && ./zbit_mk/scripts/run_all_ut.sh --max_parallel 4 --skip_top \\
; echo "Returning to ${_sd_}"                                    \\
; cd ${_sd_}'

alias ecfsw-cscope-update '${HOME}/cscope-update ${SANDBOX}'

alias ecfsw-new-sandbox '\\
    ghcore git@github.jpl.nasa.gov:ECFSW/eurcfsw.git ${PROJ_DEV}/\!:1 \\
    && set-sandbox ${PROJ_DEV}/\!:1/src \\
    && sed "s/SANDBOX_NAME_HERE/\!:1/" ${PROJ_DEV}/sandbox-cfgs/compile_commands.json > ${SANDBOX}/compile_commands.json \\
    && indir ${SANDBOX}/zbit_mk git apply ${PROJ_DEV}/sandbox-cfgs/build_ut.patch \\
    && cp ${PROJ_DEV}/sandbox-cfgs/prepare-commit-msg ${SANDBOX}/../.git/modules/src/imu_mgr/hooks/. \\
    && cp ${PROJ_DEV}/sandbox-cfgs/prepare-commit-msg ${SANDBOX}/../.git/modules/src/imu_drv/hooks/. \\
    && ln -s ${PROJ_DEV}/sandbox-cfgs/lvimrc ${SANDBOX}/.lvimrc \\
    && ln -s ${PROJ_DEV}/sandbox-cfgs/clang-format.cfg ${SANDBOX}/.clang-format'

alias ecfsw-gen-compile-db 'setenv _sd_ `pwd`                                      \\
  && echo "Entering ${SANDBOX}"                                                    \\
  && cd ${SANDBOX}                                                                 \\
  && make -C ${SANDBOX}/build_eurc clean_all                                       \\
  && rm -rf ${SANDBOX}/compile_commands.json                                       \\
  && ${HOME}/.local/bin/bear                                                       \\
      --append --output ${SANDBOX}/compile_commands.json                           \\
      --config ${PROJ_DEV}/sandbox-cfgs/bear-citnames.json                         \\
        -- ${SANDBOX}/zbit_mk/scripts/make_ut_linux.py ${SANDBOX} imu_mgr imu_drv  \\
; echo "Returning to ${_sd_}"                                                      \\
; cd ${_sd_}'
