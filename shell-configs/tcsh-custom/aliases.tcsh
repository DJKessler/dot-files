alias ls 'ls -l --color=auto'

alias vnc 'vncserver -geometry 3440x1440 -randr 2560x1440,2880x1800,3440x1440'
alias vnc-small 'xrandr -s 0'
alias vnc-mbp   'xrandr -s 1'
alias vnc-wide  'xrandr -s 2'

if ( -e "$HOME/.local/bin/zsh" ) then
  alias zsh "$HOME/.local/bin/zsh"
endif

if ( -e "$HOME/.local/bin/vim" ) then
  alias vim "$HOME/.local/bin/vim"
endif
