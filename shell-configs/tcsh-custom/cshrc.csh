set DOT_FILES = "/proj/fswcore/fsw/dev/kessler/dot-files"

if ( -e "${DOT_FILES}/shell-configs/tcsh-custom/ecfsw.tcsh" ) then
    source "${DOT_FILES}/shell-configs/tcsh-custom/ecfsw.tcsh"
endif

if ( -e "${DOT_FILES}/shell-configs/tcsh-custom/aliases.tcsh" ) then
    source "${DOT_FILES}/shell-configs/tcsh-custom/aliases.tcsh"
endif

if ( -e "${DOT_FILES}/shell-configs/tcsh-custom/git-completion.tcsh" ) then
    source "${DOT_FILES}/shell-configs/tcsh-custom/git-completion.tcsh"
endif

if ( -e "${HOME}/.git-completion.tcsh" ) then
    source "${HOME}/.git-completion.tcsh"
    set autolist=ambiguous
endif

if ( -e "${DOT_FILES}/shell-configs/tcsh-custom/prompt.tcsh" ) then
    source "${DOT_FILES}/shell-configs/tcsh-custom/prompt.tcsh"
endif

set history = (100000 '%Y-%D-%W-%T %m %R\n')
set histdup = erase
set histfile = ~/.csh_history_`hostname -s`
