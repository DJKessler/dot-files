setenv MASTER_LITE_AUTOSTART TRUE

if ( -e "/proj/fswcore/env/fswcore.env" ) then
    source "/proj/fswcore/env/fswcore.env"
    setenv PATH .:$PATH
endif

if ( -e "/proj/europa/testbed/tools/itl_cshrc" ) then
    source "/proj/europa/testbed/tools/itl_cshrc"
endif

if ( -e "${DOT_FILES}/shell-configs/tcsh-custom/ecfsw-aliases.tcsh" ) then
    source "${DOT_FILES}/shell-configs/tcsh-custom/ecfsw-aliases.tcsh"
endif

