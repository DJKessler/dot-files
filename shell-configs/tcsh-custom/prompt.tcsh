# Colors!
set     _red="%{\033[1;31m%}"
set   _green="%{\033[0;32m%}"
set  _yellow="%{\033[1;33m%}"
set    _blue="%{\033[1;34m%}"
set _magenta="%{\033[1;35m%}"
set    _cyan="%{\033[1;36m%}"
set   _white="%{\033[0;37m%}"
set     _end="%{\033[0m%}"

set prompt = "[${_red}%P${_end}] [${_green}%n${_white}@${_blue}%m${_white}:${_yellow}%~${_end}]$ "

unset _red _green _yellow _blue _magenta _cyan _white _end
