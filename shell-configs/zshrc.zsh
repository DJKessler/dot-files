shell_cfg_dir="${${${(%):-%x}:A}:h}"
dot_dir="${shell_cfg_dir:h}"

BASE16_SHELL="$shell_cfg_dir/base16-shell"
BASE16_SHELL_HOOKS="$shell_cfg_dir/base16-shell-hooks"

HISTFILE="$(dirname "${dot_dir}")/.zsh_history"
HISTSIZE=100000
SAVEHIST=1000000
setopt appendhistory beep nomatch
unsetopt autocd extendedglob notify
bindkey -e
bindkey "" history-incremental-pattern-search-backward

# enable Ctrl+arrow word movement
bindkey "^[Od" backward-word
bindkey "^[Oc" forward-word

# setup base16 color switching
[ -d "$BASE16_SHELL" ] && [ -n "$PS1" ] && [ -s "$BASE16_SHELL/profile_helper.sh" ] && source "$BASE16_SHELL/profile_helper.sh"

# set a base16 colorscheme if one is not already set
[ -d "$BASE16_SHELL" ] && [ ! -f ~/.base16_theme ] && base16_brewer

export PATH="$(getconf PATH):/usr/sbin:/usr/lib:/usr/local/bin"

# Path to your oh-my-zsh installation.
export ZSH="$shell_cfg_dir/oh-my-zsh"

# Set name of the theme to load.
ZSH_THEME="spaceship"
SPACESHIP_TIME_SHOW="true"
SPACESHIP_BATTERY_SHOW="false"

# Use case-sensitive completion.
CASE_SENSITIVE="true"

zstyle ':omz:update' mode auto      # update automatically without asking
zstyle ':omz:update' frequency 13   # update every 13 days

# Display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM="$shell_cfg_dir/zsh-custom"

# Plugins to load
plugins=(git zsh-completions zsh-autosuggestions ssh-agent pip)

# Specify the location for the completion dump file
mkdir -p "${HOME}/.cache/zsh"
export ZSH_COMPDUMP="${HOME}/.cache/zsh/.zcompdump-$(hostname -s)-${ZSH_VERSION}"
export ZSH_DISABLE_COMPFIX="true"

function source_config() {
  [[ -f "$1" ]] && source "$1"
}

# User configuration
source_config "$ZSH/oh-my-zsh.sh"
source_config "$ZSH_CUSTOM/aliases/cmake-arm.zsh"
source_config "$ZSH_CUSTOM/aliases/rsync.zsh"
source_config "$ZSH_CUSTOM/keybindings.zsh"
source_config "$ZSH_CUSTOM/macOs.zsh"
source_config "$ZSH_CUSTOM/linux.zsh"
source_config "$ZSH_CUSTOM/jpl.zsh"

if [ -d "$HOME/.local" ]; then
  export PATH="$HOME/.local/bin:$PATH"
fi

# source local configs after setting path to allow path to be overridden
source_config "$ZSH_CUSTOM/$(hostname -s).zsh"

# remove duplicates from path
typeset -U path

export GCC_COLORS='error=01;41:warning=01;93:note=01;36:caret=01;32:locus=01:quote=01'
alias ls='ls -l --color=auto'

alias dirs='dirs -v'

export GPG_TTY=$(tty)

function ssh() {
  if command ssh "$@"; then
    source "$HOME/.base16_theme"
  fi
}
