""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" ALE: https://github.com/dense-analysis/ale.git
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if (v:version) > 800
  " prevent vim automatically inserting text while I type
  set completeopt=menu,menuone,preview,noselect,noinsert

  " allows use of <Tab> to cycle through completion options "
  inoremap <silent><expr> <Tab>   pumvisible() ? "\<C-n>" : "\<TAB>"
  inoremap <silent><expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-TAB>"

  nmap <leader>ad  :ALEGoToDefinition<cr>
  nmap <leader>ar  :ALEFindReferences -quickfix<cr>
  nmap <leader>aR  :ALERepeatSelection<cr>
  nmap <leader>aa  :ALESymbolSearch<cr>
  nmap <leader>aj  :ALENextWrap<CR>
  nmap <leader>aJ  :ALEPrevious<CR>
  nmap <leader>ah  :ALEHover<cr>
  nmap <leader>ai  :ALEGoToImplementation<cr>

  let g:ale_c_build_dir_names = []
  let g:ale_c_build_dir = ''
  let g:ale_c_parse_compile_commands = 1
  let g:ale_c_parse_makefile = 0
  let g:ale_completion_enabled = 1
  let g:ale_fix_on_save = 0

  let g:ale_linters_explicit = 1

  let g:ale_c_cc_executable = '/opt/local/gcc/4.9/bin/gcc'

  let g:ale_fixers = {
  \ '*':      ['remove_trailing_lines', 'trim_whitespace'],
  \ 'sh':     ['shfmt'],
  \}

endif
