""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Standard cscope/vim boilerplate
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" use both cscope and ctag for 'ctrl-]', ':ta', and 'vim -t'
set cscopetag

" search tags before cscope
set cscopetagorder=1
set tags=./tags,tags;

" avoid the 'Added cscope database... press ENTER...' step
set nocscopeverbose
" set cscoperelative
set nocscoperelative
set tagstack

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" My cscope/vim key mappings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" The following maps all invoke one of the following cscope search types:
"
"   's'   symbol: find all references to the token under cursor
"   'g'   global: find global definition(s) of the token under cursor
"   'c'   calls:  find all calls to the function name under cursor
"   't'   text:   find all instances of the text under cursor
"   'e'   egrep:  egrep search for the word under cursor
"   'f'   file:   open the filename under cursor
"   'i'   includes: find files that include the filename under cursor
"   'd'   called: find functions that function under cursor calls

""" opens search result in a new vertical split
nmap <Leader>css :vert scs find s <C-R>=expand("<cword>")<CR><CR>
nmap <Leader>csg :vert scs find g <C-R>=expand("<cword>")<CR><CR>
nmap <Leader>csc :vert scs find c <C-R>=expand("<cword>")<CR><CR>
nmap <Leader>cst :vert scs find t <C-R>=expand("<cword>")<CR><CR>
nmap <Leader>cse :vert scs find e <C-R>=expand("<cword>")<CR><CR>
nmap <Leader>csf :vert scs find f <C-R>=expand("<cfile>")<CR><CR>
nmap <Leader>csi :vert scs find i <C-R>=expand("<cfile>")<CR><CR>
nmap <Leader>csd :vert scs find d <C-R>=expand("<cword>")<CR><CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Look for a cscope.out file somewhere between $PWD and '/'
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

function! LoadCScope()
    let s:cs_path = getcwd()
    while s:cs_path != '/'
        let s:cs_db = globpath(s:cs_path, 'cscope.out')
        let s:cs_path = fnamemodify(s:cs_path, ':h')
        if !empty(s:cs_db)
            execute "cscope add " . s:cs_db
            break
        endif
    endwhile
    return 0
endfunction

:call LoadCScope()
