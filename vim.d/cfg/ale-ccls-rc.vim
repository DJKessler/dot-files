let g:ale_c_ccls_init_options = {
\   'cache': {
\       'directory': '/tmp/ccls/cache'
\   },
\ }

let g:ale_c_ccls_executable = expand("$HOME/.local/bin/ccls")
