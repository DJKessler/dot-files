
function! s:HeaderguardReplaceSpecialChars()
  return expand('%:r:gs/[^0-9a-zA-Z_]/_/g')
endfunction

function! HeaderguardCopyrightDate()
  return strftime("%Y")
endfunction

function! s:GuardFromFilename()
  return toupper(s:HeaderguardReplaceSpecialChars())
endfunction

function! HeaderguardName()
    return s:GuardFromFilename() . "_" . toupper(expand('%:e')) . "_"
endfunction

function! HandleTemplateSkeleton()
  if &filetype=='cmake'
    if filereadable(expand("$HOME/.vim/templates/skeleton.cmake"))
      execute '0r ' . expand("$HOME/.vim/templates/skeleton.cmake")
    endif
    return
  endif

  if filereadable(expand("$HOME/.vim/templates/skeleton.").expand("<afile>:e"))
    execute '0r ' . expand("$HOME/.vim/templates/skeleton.").expand("<afile>:e")
  endif
endfunction

augroup templates
  autocmd!
  autocmd BufNewFile *.h,*.c,*.py,*.sh,*CMakeLists.txt,*.cmake call HandleTemplateSkeleton()
  autocmd BufNewFile *.h,*.c,*.py,*.sh,*CMakeLists.txt,*.cmake %substitute#\[:VIM_EVAL:\]\(.\{-\}\)\[:END_EVAL:\]#\=eval(submatch(1))#ge
augroup END
