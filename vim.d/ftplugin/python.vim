let b:ale_linters = ['pyls', 'mypy']
let b:ale_fixers = ['autopep8', 'yapf', 'trim_whitespace']
