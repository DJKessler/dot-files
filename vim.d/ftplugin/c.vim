" let b:ale_linters = ['ccls']
let b:ale_linters = ['cc', 'clangd']
let b:ale_fixers = ['clang-format', 'clangtidy', 'remove_trailing_lines', 'trim_whitespace']
let b:ale_linters_ignore = ['ccls', 'clangtidy', 'cppcheck', 'cpplint', 'cquery', 'cspell', 'flawfinder']

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""" CCLS """""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if filereadable(expand("$HOME/.vim/cfg/ale-ccls-rc.vim"))
  source $HOME/.vim/cfg/ale-ccls-rc.vim
endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""""" CLANGD """"""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if filereadable(expand("$HOME/.vim/cfg/ale-clangd-rc.vim"))
  source $HOME/.vim/cfg/ale-clangd-rc.vim
endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""" CLANG-TIDY """"""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if filereadable(expand("$HOME/.vim/cfg/ale-clang-tidy-rc.vim"))
  source $HOME/.vim/cfg/ale-clang-tidy-rc.vim
endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""""" CSCOPE """"""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if filereadable(expand("$HOME/.vim/cfg/cscope-rc.vim"))
  source $HOME/.vim/cfg/cscope-rc.vim
endif

